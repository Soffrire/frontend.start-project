window.sliderSettings = [
  {
    // Параметры слайдера js-slider
    class: '.js-slider',
    settings: {
      slidesPerView: 1,
      loop: true,
      grabCursor: true,
      effect: 'creative',
      creativeEffect: {
        prev: {
          shadow: true,
          translate: ['-120%', 0, -500],
        },
        next: {
          shadow: true,
          translate: ['120%', 0, -500],
        },
      },
      navigation: {
        prevEl: null,
        nextEl: '.js-cases-slider-btn-next'
      },
      pagination: {
        el: '.js-cases-slider-pagination',
        bulletClass: 'cases-slider__pagination-bullet',
        bulletActiveClass: 'cases-slider__pagination-bullet--active'
      }
    }
  },
]
