/* eslint-disable no-undef */
const mapHandler = {
  init() {
    if ($('#js-map-render').length === 0) return

    if (typeof ymaps === 'undefined')
      this.addScript()
    else
      this.initMap()
  },

  addScript() {
    const script = document.createElement('script')
    script.src = '//api-maps.yandex.ru/2.1/?lang=ru_RU&apikey=ddd3730b-6500-4ad2-8af2-36d72cbbca77'
    script.async = true
    script.onload = () => {
      this.initMap()
    }
    document.head.appendChild(script)
  },

  initMap() {

    ymaps.ready(() => {
      this.map = new ymaps.Map('js-map-render', {
        center: [55.389714, 52.196639],
        zoom: 8,
        behaviors: ['default', 'scrollZoom'],
        controls: []
      })
    })

  }
}

const initMaps = () => {
  mapHandler.init()
}

export default initMaps
